/*
 * Main.cpp
 *
 *  Created on: Fall 2019
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>
#include <CImg.h>
#include <errno.h>
#include <time.h>

using namespace cimg_library;

// Data type for image components
// FIXME: Change this type according to your group assignment
typedef double data_t;
const char* SOURCE_IMG      = "bailarina.bmp";
const char* DESTINATION_IMG = "bailarina2.bmp";
const char* MIX_IMG = "background_V.bmp";
const int numThreads = 8;
CImg<data_t> srcImage(SOURCE_IMG);
CImg<data_t> mixImage(MIX_IMG);
data_t *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components
data_t *pRmix, *pGmix,*pBmix;
data_t *pRdest, *pGdest, *pBdest;
data_t *pDstImage; // Pointer to the new image pixels
uint width, height; // Width and height of the image
uint nComp; // Number of image components

//Funcion de los hilos
void* Ennegrecer(void* arg){
	int n = *((int*)arg);

	width  = srcImage.width(); // Getting information from the source image
	height = srcImage.height();
	nComp  = srcImage.spectrum();

	pRsrc = srcImage.data(); // pRcomp points to the R component array
	pGsrc = pRsrc + height * width; // pGcomp points to the G component array
	pBsrc = pGsrc + height * width; // pBcomp points to B component array

	// Pointers to the RGB arrays of the destination image
	pRdest = pDstImage;
	pGdest = pRdest + height * width;
	pBdest = pGdest + height * width;
	
	//Pointers to the component arrays of the mix image
	pRmix = mixImage.data();
	pGmix = pRmix + height * width;
	pBmix = pGmix + height * width;

	int index = height/numThreads;
	int fin = index * n;
	int inicio = index * (n-1);
	uint j = 0;
	while(j<20){
		for (uint i = width * inicio; i < width * fin; i++){
			pRdest[i] = 255 - ((256 * (255 - pRmix[i])) / (pRsrc[i] + 1));  // This is equals to pRdest[i] = pGsrc[i]
			if (pRdest[i] < 0){
				pRdest[i] = 0;
			}

			pGdest[i] = 255 - ((256 * (255 - pGmix[i])) / (pGsrc[i] + 1));
			if (pGdest[i] < 0){
				pGdest[i] = 0;
			}

			pBdest[i] = 255 - ((256 * (255 - pBmix[i])) / (pBsrc[i] + 1));
			if (pBdest[i] < 0){
				pBdest[i] = 0;
			}
		}
		j++;
	}
	return NULL;
}




int main() {
	// Open file and object initialization
	srcImage.display(); // Displays the source image
	width  = srcImage.width(); // Getting information from the source image
	height = srcImage.height();
	nComp  = srcImage.spectrum();

	pDstImage = (data_t *) malloc (width * height * nComp * sizeof(data_t));
	if (pDstImage == NULL) {
		perror("Allocating destination image");
		exit(-2);
	}
	
	pthread_t thread[numThreads];
    int a[numThreads];
	/***********************************************
	 * TODO: Algorithm start.
	 *   - Measure initial time
	 */
	struct timespec tStart, tEnd;
    double dElapsedTimeS;

	// Comieza a medir tiempo:
    printf("Running task    : \n");
    if(clock_gettime(CLOCK_REALTIME, &tStart)){
		    printf("ERROR: clock_gettime: %d.\n", errno);
            exit(EXIT_FAILURE);
	}

	//Creacion Hilos
	for (uint i = 0; i < numThreads; i++)
    {
        a[i]=i;
        if (pthread_create(&thread[i], NULL, Ennegrecer, &a[i]) != 0)
        {
            fprintf(stderr, "ERROR: Creating thread %d\n", i);
            return EXIT_FAILURE;
        }
    }

	//Esperamos a que se completen hilos
	printf("Main thread waiting...\n");
    for (uint i = 0; i < numThreads; i++)
    {
        pthread_join(thread[i], NULL);
    }
    printf("Main thread finished.\n");
	/***********************************************
	 * TODO: End of the algorithm.
	 *   - Measure the end time
	 *   - Calculate the elapsed time
	 */
	if(clock_gettime(CLOCK_REALTIME, &tEnd)){
			printf("ERROR: clock_gettime: %d.\n", errno);
            exit(EXIT_FAILURE);
	}

	dElapsedTimeS = (tEnd.tv_sec - tStart.tv_sec);
    dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;
	printf("Elapsed time    : %f s.\n", dElapsedTimeS);
		
	// Create a new image object with the calculated pixels
	// In case of normal color images use nComp=3,
	// In case of B/W images use nComp=1.
	CImg<data_t> dstImage(pDstImage, width, height, 1, nComp);

	// Store destination image in disk
	dstImage.save(DESTINATION_IMG); 

	// Display destination image
	dstImage.display();

	return 0;
}
